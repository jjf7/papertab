*! 0.0.10 Jerome Federspiel 19apr2013

/* Revisions in 0.0.10 ************************/
*  1. Stdiff stat option
/*********************************************/

/* Revisions in 0.0.9 ************************/
*  1. RTF export
/*********************************************/

/* Revisions in 0.0.8 ************************/
*  1. Switched append support to by tables
*  2. Added stars option
/*********************************************/

/* Revisions in 0.0.7 ************************/
*  1. Added append support
*  2. Fixed issue where outtab did not save file (hopefully for real)
/*********************************************/

/* Revisions in 0.0.6 ************************/
*  1. Added if/in support
*  2. Fixed issue where outtab did not save file
*  3. Added subpop() options to all svy: commands
*  4. Added weighted N to svy output.
/*********************************************/

// Main program
program papertab, rclass
  version 11

  syntax varlist(numeric fv) [if] [in], TREATvar(varname numeric) [title(string) OUTTABle(string) OUTRTF(string) STARs STATtype(string) LEVELoverride PCTFmt(string) CONTFmt(string) COLWidth(int 25) LABWidth(int 35) SVY]

  quietly {

  * Expand variable list
  fvunab varlist2: `varlist'
  local wc: word count `varlist2'

  marksample touse,  novarlist

  * Ensure no interaction terms in varlist
  local strposhash = strpos("`varlist'", "#")
  if `strposhash' > 0 {
    noisily di as err "No interaction terms permitted in variable list (use i. operator only)"
    exit 198
  }

  * Ensure appropriate stattype
  if inlist("`stattype'","parametric","nonparm","trend","stdiff","") == 0 {
    noisily di as err `"If specified, stattype() must be "parametric","nonparm", "stdiff", or "trend" "'
    exit 198
  }

  * Get levels of treatment variable
  levelsof `treatvar' if `touse', local(tvlevels)
  local tvlevelcount : word count `tvlevels'

  * Reject stdiff if >2 levels
  if "`stattype'" == "stdiff" & `tvlevelcount' != 2 {
    noisily di as err `"treatvar() variable `treatvar' has `tvlevelcount' unique value(s) (`tvlevels'). Can only run stdiff with treatvars with exactly 2 levels."'
    exit 198
  }

  * Sanity check for levels of treatment variable
  if `tvlevelcount' > 10 & "`leveloverride'" != "leveloverride" {
    noisily di as err `"treatvar() variable `treatvar' has > 10 unique values. If not a mistake, add leveloverride option. "'
    exit 198
  }

  * Enable survey functionality
  local weighted
  if "`svy'" == "svy" {
    local weighted = "weighted"
  }
  
  * Only run survey-weighted stats if stattype = parametric or "stdiff"
  if inlist("`stattype'","parametric","stdiff")==0 & "`weighted'"=="weighted" {
    noisily di as err `"Can only use weighted data with stattypes "nonparm" and "stdiff" "'
    exit 198
  }

  * Display warning if missing levels of treatvar
  capture assert missing(`treatvar') == 0 if `touse'
  if _rc == 9 {
    count if missing(`treatvar') == 1 & `touse'
    local msn = r(N)
    local warntreat = "Overall total != group sum because `msn' obs w/ missing values for treatvar() variable `treatvar'"
  }

  local warnlist
  local warncount = 1
  * Count if missing values
  forvalues i = 1/`wc'  {
  	local x: word `i' of `varlist2'
    fvrevar `x', list
    local y `r(varlist)'
    capture assert missing(`y') == 0 if `touse'
    if _rc == 9 {
      if `warncount' == 1 {
        local warnlist "Missing values in"
      }
      local warncount = `warncount' + 1
      count if missing(`y') == 1 & `touse'
      local msn = r(N)
      local warnlist "`warnlist' `y' (`msn' obs)"
    }
  }

  * Get variable label for treatvar
  local tv_varlab: variable label `treatvar'

  if "`tv_varlab'" == "" {
     local tv_varlab = "`treatvar'"
  }

  * Get value labels for treatvar
  local tv_vallab
  foreach x in `tvlevels' {
     local tv_vallab_`x': label (`treatvar') `x'
  }

  * Set up replace for outtable
  local 0 "`outtable'"
  syntax [anything(name=outtable)] [ , replace append]
  if "`outtable'" == "" {
    local replab
    local appendlab
  }
  if "`replace'" == "replace" {
      local replab replace
  }
  if "`append'" == "append" {
      local appendlab append
  }

  if "`replace'" == "replace" & "`append'" == "append" {
   di as err "Cannot replace and append at same time"
   exit 198
  }

  * Set up replace for outrtf
  local 0 "`outrtf'"
  syntax [anything(name=outrtf)] [ , replace rtflabelwidth(real 2) rtfcolwidth(real 1.0) rtfpwidth(real 0.75)]
  if "`outrtf'" == "" {
    local replabrtf
    local appendlabrtf
  }
  if "`replace'" == "replace" {
      local replabrtf replace
  }

  * Set up formats
  if "`pctfmt'" == "" {
    local pctfmt "%4.1f"
  }

  if "`contfmt'" == "" {
    local contfmt "%4.1f"
  }

  local rtflabin = `rtflabelwidth'*1440
  local rtfcolin = `rtfcolwidth'*1440
  local rtfpvin = `rtfpwidth'*1440

  * Set up output
  * Beginning of output
  local colcount = `labwidth'
  local dilabel `" _column(`labwidth') "Overall" "'
  count if `touse'
  local count1 = string(r(N),"%12.0fc")
  local count1oa `count1'
  local dilabel2 `" _column(`labwidth') "N = `count1'" "'
  if "`weighted'" == "weighted" {
    tempvar counter
    gen `counter'=1
    svy, subpop(`touse'): total `counter'
    local count = string(e(N_subpop), "%12.0fc")
    local dilabel3 `" _column(`labwidth') "Wgt. N = `count'" "'
    local add2outoa " (Weighted N = `count')"
  }
  local distate `" _column(`labwidth') r(comb_oa) "'
  if "`add2outoa'" != "" {
    local outlabel `" _tab "Overall (N= `count1')\par `add2outoa'" "'
  }
  if "`add2outoa'" != "" {
    local outlabel `" _tab "Overall (N= `count1')" "'
  }
  local outstate `" _tab (r(comb_oa)) "'
  local numlevels : word count `tvlevels'
  local rtfrow1 "\trowd\trgaph144\clbrdrt\brdrs\cellx`rtflabin'\clbrdrt\brdrs\clbrdrb\brdrs\cellx`=`rtflabin'+(`numlevels'+1)*`rtfcolin'+`rtfpvin''"
  local rtfrow2 "\trowd\trgaph144\clbrdrb\brdrs\cellx`rtflabin'\clbrdrb\brdrs\cellx`=`rtflabin'+`rtfcolin''\"
  local rtfrow3 "\trowd\trgaph144\cellx`rtflabin'\cellx`=`rtflabin'+`rtfcolin''\"

  local lagval = `=`rtflabin'+`rtfcolin''


  * Levels of treatvar
  foreach x in `tvlevels' {
    local colcount = `colcount'+`colwidth'
    local dilabel `" `dilabel'  _column(`colcount') (substr("`tv_vallab_`x''",1,`=`colwidth'-5')) "'
    local distate `" `distate' _column(`colcount') r(comb_`x') "'
    count if `treatvar'==`x' & `touse'
    local count1 = string(r(N),"%12.0fc")
    if "`weighted'" == "weighted" {
      tempvar counter
      gen `counter'=1
      tempvar include
      gen `include' = `touse' & `treatvar' == `x'
      svy, subpop(`include'): total `counter'
      local count = string(e(N_subpop), "%12.0fc")
      local dilabel3 `"`dilabel3' _column(`colcount') _column(`labwidth') "Wgt. N = `count'" "'
      local add2out " (Weighted N = `count')"
    }
    local dilabel2 `" `dilabel2' _column(`colcount') "N = `count1'" "'
    local outlabel `" `outlabel' _tab "`tv_vallab_`x'' (N= `count1')`add2out'"  "'
    local outstate `" `outstate' _tab (r(comb_`x')) "'
    local rtfrow2  `"`rtfrow2'clbrdrb\brdrs\cellx`=`lagval'+`rtfcolin''\"'
    local rtfrow3  `"`rtfrow3'cellx`=`lagval'+`rtfcolin''\"'
    local lagval = `lagval'+`rtfcolin'
  }

  local rtfrow3 `"`rtfrow3'cellx`=`lagval'+`rtfpvin''"'

  * P-value
  local colcount = `colcount'+`colwidth'

  if "`stattype'" != "stdiff" {
    local dilabel2 `" `dilabel2'  _column(`colcount') "p" "'
    local outlabel `" `outlabel' _tab "p" "'
  }

  else {
    local dilabel2 `" `dilabel2'  _column(`colcount') "st. diff." "'
    local outlabel `" `outlabel' _tab "st. diff." "'
  }

  local distate `" `distate'  "'
  local outstate `" `outstate' "'

  * Print header data
  if "`title'" != "" {
    noisily di "`title'"
  }
  noisily display "{txt}{hline `=`colcount'+5'}"
  noisily di _column(`labwidth') "`tv_varlab'"
  noisily di `dilabel'
  noisily di `dilabel2'
  if "`weighted'" == "weighted" {
    noisily di `dilabel3'
  }
  noisily display "{txt}{hline `=`colcount'+5'}"

  if "`stattype'"=="nonparm" | "`stattype'"=="trend" {
    noisily display _column(`=floor((`colcount')/2)-5') "Median [25th, 75th Percentile] or %"
  }

  else {
    noisily display _column(`labwidth') "Mean (Standard Deviation) or %"
  }

  if "`outtable'" != "" {

    tempname myfile
    file open `myfile' using "`outtable'", write `replab' `appendlab'

    if "`title'" != "" {
      file write `myfile' "`title'" _n
    }
    file write `myfile' _tab "`tv_varlab'" _n
    file write `myfile' `outlabel' _n

    if "`stattype'"=="nonparm" | "`stattype'"=="trend" {
      file write `myfile' _tab "Median [25th, 75th Percentile] or %" _n
    }

    else {
      file write `myfile' _tab "Mean (Standard Deviation) or %" _n
    }
  }

  if "`outrtf'" != "" {
    tempname rtffile
    file open `rtffile' using "`outrtf'", write `replabrtf'


    file write `rtffile' "{\rtf1\ansi\deff0" _n "{\fonttbl" _n "{\f0 Times New Roman;}" _n "{\f1 Symbol;}" _n "}" _n "\paperw15840\paperh12240\margl720\margr720\margt720\margb720" _n
    if "`title'" != "" {
      file write `rtffile' "\trowd\trgaph144\cellx`=`lagval'+`rtfpvin''" _n
      file write `rtffile' "\pard\intbl\ql `title'\cell" _n "\row" _n

    }
    file write `rtffile' "`rtfrow1'" _n
    file write `rtffile' "\pard\intbl\ql  \cell" _n "\pard\intbl\qc `tv_varlab' \cell" _n "\row" _n "`rtfrow2'clbrdrb\brdrs\cellx`=`lagval'+`rtfpvin''" _n
    file write `rtffile' "\pard\intbl\ql  \cell" _n "\pard\intbl\qc Overall\par (N=`count1oa')`add2outoa'\cell" _n
    foreach x in `tvlevels' {
      count if `treatvar'==`x' & `touse'
      local count1 = string(r(N),"%12.0fc")
      local add2out
      if "`weighted'" == "weighted" {
      tempvar counter
        gen `counter'=1
        tempvar include
        gen `include' = `touse' & `treatvar' == `x'
        svy, subpop(`include'): total `counter'
        local count = string(e(N_subpop), "%12.0fc")
        local add2out "\par (Weighted N = `count')"
      }

      file write `rtffile' "\pard\intbl\qc `tv_vallab_`x''\par (N=`count1')`add2out'\cell" _n
    }
    
    if "`stattype'" != "stdiff" {
      file write `rtffile' "\pard\intbl\qc p\cell" _n "\row" _n "\trowd\trgaph144\cellx`rtflabin'\cellx`lagval'\cellx`=`lagval'+`rtfpvin''" _n "\pard\intbl\ql  \cell" _n
    }
    else {
      file write `rtffile' "\pard\intbl\qc Abs. SD\cell" _n "\row" _n "\trowd\trgaph144\cellx`rtflabin'\cellx`lagval'\cellx`=`lagval'+`rtfpvin''" _n "\pard\intbl\ql  \cell" _n
    }
   
    if "`stattype'"=="nonparm" | "`stattype'"=="trend" {
      file write `rtffile' "\pard\intbl\qc Median [25th-75th Percentile] or % \cell" _n "\pard\intbl\ql  \cell" _n "\row" _n
    }
    else {
      file write `rtffile' "\pard\intbl\qc Mean (Standard Deviation) or % \cell" _n "\pard\intbl\ql  \cell" _n "\row" _n
    }


  }

  * Run through variables

  forvalues i = 1/`wc' {

    local xwfactor : word `i' of `varlist2'
    fvexpand `xwfactor'
    local factorresult = r(fvops)

    * If not factor variable, calculate directly
    if "`factorresult'" != "true" {

      fvrevar `xwfactor', list
      local x `r(varlist)'

      * Run stats
      if "`stattype'" != "stdiff" {
        papertab_runstat `x' if `touse', treatvar(`treatvar') stattype(`stattype') pctfmt(`pctfmt') contfmt(`contfmt') `weighted' `stars'
      }
      
      else {
        papertab_runsd `x' if `touse', treatvar(`treatvar') pctfmt(`pctfmt') contfmt(`contfmt') `weighted' 
      }
      
      * Get variable label (use variable name if no label)
      local varlab: variable label `x'
      if "`varlab'" == "" {
        local varlab = "`x'"
      }

      noisily di (substr("`varlab'",1,`=`labwidth'-5')) `distate' _column(`colcount') r(pform)

      if "`outtable'" != "" {
        file write `myfile' "`varlab'" `outstate' _tab (r(pform)) _n
      }

      if "`outrtf'" != "" {
        file write `rtffile' "`rtfrow3'" _n
        file write `rtffile' "\pard\intbl\ql `varlab'\cell" _n
        file write `rtffile' "\pard\intbl\qc `r(comb_oa)'\cell" _n
          foreach x in `tvlevels' {
           file write `rtffile' "\pard\intbl\qc `r(comb_`x')'\cell" _n
          }
        file write `rtffile' "\pard\intbl\qc `r(pform)'\cell" _n
        file write `rtffile' "\row" _n
      }
    }

    * If factor variable, more work to do
    else {

      fvrevar `xwfactor', list
      local x `r(varlist)'

      * Get variable label (uses variable name if no label)
      local varlab: variable label `x'
      if "`varlab'" == "" {
        local varlab = "`x'"
      }

      * Count levels of variable
      levelsof `x' if `touse', local(levels)
      local levelcount: word count `levels'

      local addlab
      local start = 1

      if "`stattype'" != "trend" &  "`stattype'" != "stdiff" {
        local disppv
        local outpv
        local outpvrtf
      }

      else {
        local disppv _column(`colcount') r(pform)
        local outpv _tab (r(pform))
        local outpvrtf "\`r(pform)'"
      }

      if "`stattype'" != "trend" & "`stattype'" != "stdiff" {
        if "`weighted'" == "weighted" {
          quietly svy, subpop(`touse'): tab `x' `treatvar'
          papertab_fmtpv, indat(`=e(p_Pear)') `stars'
        }
        else {
          quietly tab `x' `treatvar' if `touse', chi2
          papertab_fmtpv, indat(`=r(p)') `stars'
        }

        local pvform = r(pform)
      }

      noisily di "`varlab'" _column(`colcount') "`pvform'"
      local tabspace
      if "`outtable'" != "" {
        forvalues i = 1 / `=`tvlevelcount'+2' {
          local tabspace = "`tabspace' _tab"
        }
        file write `myfile' "`varlab'" `tabspace' "`pvform'" _n
      }

      if "`outrtf'" != "" {
        file write `rtffile' "`rtfrow3'" _n
        file write `rtffile' "\pard\intbl\ql `varlab'\cell" _n
        file write `rtffile' "\pard\intbl\qc \cell" _n
        foreach q in `tvlevels' {
           file write `rtffile' "\pard\intbl\qc \cell" _n
        }
        file write `rtffile' "\pard\intbl\qc `pvform'\cell" _n
        file write `rtffile' "\row" _n
      }

      forvalues i = `start'/`levelcount' {
        local z : word `i' of `levels'

        * Generate indicator variable for value
        tempvar `x'_`z'
        gen ``x'_`z'' = (`x' == `z') if !missing(`x')

        * Get value label (uses numeric value if no label)
        local vallab: label (`x') `z'

        * Combine variable and value labels together
        if "`vallab'" != "" {
          local comblab "`varlab': `vallab'"
        }

        else {
          local comblab "`varlab'"
        }
        
        if "`stattype'" != "stdiff" {
          papertab_runstat ``x'_`z'' if `touse', treatvar(`treatvar') stattype(`stattype') pctfmt(`pctfmt') contfmt(`contfmt') skippv `weighted' `stars'
        }
        else {
          papertab_runsd ``x'_`z'' if `touse', treatvar(`treatvar') pctfmt(`pctfmt') contfmt(`contfmt') `weighted' 
        }
        
        * Output
        noisily di substr("`addlab'",1,`=`labwidth'-5') _column(2) (substr("`vallab'",1,30)) `distate' `disppv'

        if "`outtable'" != "" {
          file write `myfile' "`addlab'" "`vallab'" `outstate' `outpv' _n
        }

        if "`outrtf'" != "" {
          file write `rtffile' "`rtfrow3'" _n
          file write `rtffile' "\pard\intbl\li200\ql `vallab'\cell" _n
          file write `rtffile' "\pard\intbl\qc `r(comb_oa)'\cell" _n
          foreach tvlev in `tvlevels' {
            file write `rtffile' "\pard\intbl\qc `r(comb_`tvlev')'\cell" _n
          }
          file write `rtffile' "\pard\intbl\qc `outpvrtf'\cell" _n
          file write `rtffile' "\row" _n
        }

      }

     }

  }

  * Close up
  if "`outtable'" != "" {
    if "`stattype'"=="nonparm" {
      file write `myfile' "P-values by Kruskal-Wallis test for continuous variables and chi2 test for binary / categorical variables"
    }
    else if "`stattype'"=="trend" {
       file write `myfile' "P-values by non-parametric test for trend"
    }
    else if "`stattype'"=="stdiff" {
       file write `myfile' "Absolute standardized differences are expressed as percentages"
    }
    else if "`weighted'" == "weighted" {
       file write `myfile' "P-values by weighted linear regression for continuous variables and weighted chi2 test for binary/categorical variables"
    }
    else {
      if `tvlevelcount' == 2 {
        file write `myfile' "P-values by t-test for continuous variables and chi2 test for binary / categorical variables"
      }
      else if `tvlevelcount' > 2 {
        file write `myfile' "P-values by ANOVA for continuous variables and chi2 test for binary / categorical variables"
      }
    }

    if "`warntreat'" != "" {
      file write `myfile' _n "`warntreat'"
    }

    if "`warnlist'" != "" {
      file write `myfile' _n "`warnlist'"
    }

    if "`stars'" == "stars" {
      file write `myfile' _n "* p<0.05, ** p<0.01, *** p<0.001"
    }


    file write `myfile' _n _n
    file close `myfile'
  }

  noisily display "{txt}{hline `=`colcount'+5'}"

  if "`stattype'"=="nonparm" {
    noisily display "P-values by Kruskal-Wallis test for continuous variables and chi2 test for binary/categorical variables"
  }
  else if "`stattype'"=="trend" {
      noisily display "P-values by non-parametric test for trend"
  }
  else if "`stattype'"=="stdiff" {
      noisily display "Absolute standardized differences are expressed as percentages"
  }
  else if "`weighted'" == "weighted" {
      noisily display "P-values by weighted linear regression for continuous variables and weighted chi2 test for binary/categorical variables"
      noisily display `""subpop()" used instead of "if" for all svy: commands"'
  }
  else {
    if `tvlevelcount' == 2 {
      noisily display "P-values by t-test for continuous variables and chi2 test for binary/categorical variables"
    }
    else if `tvlevelcount' > 2 {
      noisily display "P-values by ANOVA for continuous variables and chi2 test for binary/categorical variables"
    }
  }
  if "`warntreat'" != "" {
    noisily display "`warntreat'"
  }

  if "`warnlist'" != "" {
    noisily display "`warnlist'"
  }

  if "`stars'" == "stars" {
    noisily display "* p<0.05, ** p<0.01, *** p<0.001"
  }

  if "`outrtf'" != "" {
    file write `rtffile' "\trowd\trgaph144\clbrdrt\brdrs\cellx`=`lagval'+`rtfpvin''"
    file write `rtffile' "\pard\intbl\ql "
    if "`stattype'"=="nonparm" {
      file write `rtffile' "P-values by Kruskal-Wallis test for continuous variables and chi2 test for binary / categorical variables"
    }
    else if "`stattype'"=="trend" {
       file write `rtffile' "P-values by non-parametric test for trend"
    }
    else if "`stattype'"=="stdiff" {
      file write `rtffile' "Absolute standardized differences are expressed as percentages"
    }
    else if "`weighted'" == "weighted" {
       file write `rtffile' "P-values by weighted linear regression for continuous variables and weighted chi2 test for binary/categorical variables"
    }
    else {
      if `tvlevelcount' == 2 {
        file write `rtffile' "P-values by t-test for continuous variables and chi2 test for binary / categorical variables"
      }
      else if `tvlevelcount' > 2 {
        file write `rtffile' "P-values by ANOVA for continuous variables and chi2 test for binary / categorical variables"
      }
    }

    if "`warntreat'" != "" {
      file write `rtffile' "\par `warntreat'"
    }

    if "`warnlist'" != "" {
      file write `rtffile' "\par `warnlist'"
    }

    if "`stars'" == "stars" {
      file write `rtffile' "\par * p<0.05, ** p<0.01, *** p<0.001"
    }

    file write `rtffile' "\cell" _n "\row" _n
    file write `rtffile' "}"
    file close `rtffile'
  }
}
end papertab

// Subroutine to run stats on individual variables
program papertab_runstat, rclass
  version 11
  syntax varname [if] [in], treatvar(varname) pctfmt(string) contfmt(string) [stattype(string) weighted skippv stars]

  * Generate levels of treatment variable
  quietly levelsof `treatvar', local(tvlevels)
  local levelcount : word count `tvlevels'

  marksample touse

  * Identify if analysis variable is binary
  capture assert (`varlist' == 0 | `varlist' == 1 | `varlist' == .) if `touse'

  * If not binary
  if _rc == 9 {

    * Parametric Analysis
    if "`stattype'"=="parametric" | "`stattype'"=="" {
      if "`weighted'" == "weighted" {
        svy, subpop(`touse'): qui mean `varlist'
        estat sd
        tempname meanoa sdoa
        matrix `meanoa' = r(mean)
        matrix `sdoa' = r(sd)
        return scalar m_oa = `meanoa'[1,1]
        local meanoaval = `meanoa'[1,1]
        return scalar s_oa = `sdoa'[1,1]
        local soaval = `sdoa'[1,1]
        return local comb_oa = string(`meanoaval',"`contfmt'")+" ("+string(`soaval',"`contfmt'")+")"

        svy, subpop(`touse'): qui mean `varlist', over(`treatvar')
        estat sd
        tempname meanvar sdvar
        matrix `meanvar' = r(mean)
        matrix `sdvar' = r(sd)
        local i = 1

        foreach x in `tvlevels' {
          return scalar m_`x' = `meanvar'[1,`i']
          local m_`x' = `meanvar'[1,`i']
          return scalar s_`x' = `sdvar'[1,`i']
          local s_`x' = `sdvar'[1,`i']
          return local comb_`x' = string(`m_`x'',"`contfmt'")+" ("+string(`s_`x'',"`contfmt'")+")"
          local i = `i'+1
        }

        svy, subpop(`touse'): reg `varlist' i.`treatvar'
        testparm i.`treatvar'
        papertab_fmtpv, indat(`=r(p)') `stars'
        return local pform = r(pform)

      }

      else {
        qui sum `varlist' if `touse'
        return scalar m_oa = r(mean)
        return scalar s_oa = r(sd)
        return local comb_oa = string(r(mean),"`contfmt'")+" ("+string(r(sd),"`contfmt'")+")"

        local i = 1
        foreach x in `tvlevels' {
          qui sum `varlist' if `treatvar'==`x' & `touse'
          return scalar m_`x' = r(mean)
          return scalar s_`x' = r(sd)
          return local comb_`x' = string(r(mean),"`contfmt'")+" ("+string(r(sd),"`contfmt'")+")"
          local i = `i'+1
        }

        * Use t-test if binary treatment group, ANOVA otherwise
        if `levelcount' == 2 {
          quietly ttest `varlist' if `touse', by(`treatvar')
          return scalar p = r(p)
          papertab_fmtpv, indat(`=r(p)') `stars'
          return local pform = r(pform)
        }

        else {
          quietly anova `varlist' `treatvar' if `touse'
          return scalar p = 1-F(e(df_m),e(df_r),e(F_1))
          papertab_fmtpv, indat(`=1-F(e(df_m),e(df_r),e(F_1))') `stars'
          return local pform = r(pform)
        }
      }
    }

    * Non-Parametric Analysis
    else {

      _pctile `varlist' if `touse', percentiles(25 50 75)
      return scalar p25_oa = r(r1)
      return scalar p50_oa = r(r2)
      return scalar p75_oa = r(r3)
      return local comb_oa = string(r(r2),"`contfmt'")+" ["+string(r(r1),"`contfmt'")+", "+string(r(r3),"`contfmt'")+"]"

      foreach x in `tvlevels' {
        _pctile `varlist' if `treatvar' == `x' & `touse', percentiles(25 50 75)
        return scalar p25_`x' = r(r1)
        return scalar p50_`x' = r(r2)
        return scalar p75_`x' = r(r3)
        return local comb_`x' = string(r(r2),"`contfmt'")+" ["+string(r(r1),"`contfmt'")+", "+string(r(r3),"`contfmt'")+"]"
      }

      if "`stattype'" == "nonparm" {
        qui kwallis `varlist' if `touse', by(`treatvar')
        return scalar p = 1-chi2(r(df),r(chi2_adj))
        papertab_fmtpv, indat(`=1-chi2(r(df),r(chi2_adj))') `stars'
        return local pform = r(pform)
      }

      else if "`stattype'" == "trend" {
        quietly nptrend `varlist' if `touse', by(`treatvar')
        return scalar p = r(p)
        papertab_fmtpv, indat(`=r(p)') `stars'
        return local pform = r(pform)
      }
    }
  }

  * If binary variable
  else {
    if "`weighted'"=="weighted" {
      svy, subpop(`touse'): qui mean `varlist'
      tempname oamean
      matrix `oamean' = e(b)
	  local n_oa = el(e(_N_subp),1,1)*`oamean'[1,1]
      return scalar pr_oa = `oamean'[1,1]
      local pr_oa = `oamean'[1,1]*100
      return local comb_oa = string(`n_oa',"%9.0fc")+" ("+string(`pr_oa',"`pctfmt'")+")"

      svy, subpop(`touse'): qui mean `varlist', over(`treatvar')
      tempname varmean
      matrix `varmean' = e(b)
      local i = 1
      foreach x in `tvlevels' {
        return scalar pr_`x' = `varmean'[1,`i']
        local pr_`x' = `varmean'[1,`i']*100
		local n_`x' = el(e(_N_subp),1,`i')*`varmean'[1,`i']
        return local comb_`x' = string(`n_`x'',"%9.0fc")+" ("+string(`pr_`x'',"`pctfmt'")+")"
        local i = `i'+1
      }

      if "`skippv'" == "skippv" {
      }
      else {
        svy, subpop(`touse'): tab `varlist' `treatvar'
        return scalar p = e(p_Pear)
        papertab_fmtpv, indat(`=e(p_Pear)') `stars'
        return local pform = r(pform)
      }
    }

    else {
      quietly sum `varlist' if `touse'

      return scalar pr_oa = r(mean)
	  local n_oa = r(N)*r(mean)
      local pr_oa = r(mean)*100
      return local comb_oa = string(`n_oa',"%9.0fc")+" ("+string(`pr_oa',"`pctfmt'")+")"

      local i = 1
      foreach x in `tvlevels' {
      	qui sum `varlist' if `treatvar'==`x' & `touse'
		local n_`x' = r(N)*r(mean)
        return scalar pr_`x' = r(mean)
        local pr_`x' = r(mean)*100
        return local comb_`x' = string(`n_`x'',"%9.0fc")+" ("+string(`pr_`x'',"`pctfmt'")+")"
        local i = `i'+1
      }

      if ("`stattype'" == "nonparm" | "`stattype'"=="parametric" | "`stattype'"=="" ) {
        qui tab `varlist' `treatvar' if `touse', chi2
        return scalar p = r(p)
        papertab_fmtpv, indat(`=r(p)') `stars'
        return local pform = r(pform)
      }

      else if "`stattype'" == "trend" {
        quietly nptrend `varlist' if `touse', by(`treatvar')
        return scalar p = r(p)
        papertab_fmtpv, indat(`=r(p)') `stars'
        return local pform = r(pform)
      }
    }
  }

end papertab_runstat

// Subroutine for formatting p-values
program define papertab_fmtpv, rclass
  syntax , indat(real) [stars]

  if `indat' < 0.001 {
    if "`stars'"=="stars" {
      return local pform = "<0.001***"
    }
    else {
      return local pform = "<0.001"
    }
  }

  else if `indat' < 0.01 {
    if "`stars'"=="stars" {
      return local pform = string(`indat', "%5.3f")+"**"
    }
    else {
      return local pform = string(`indat', "%5.3f")
    }
  }

  else if `indat' > 0.99 {
    return local pform = ">0.99"
  }

  else {
    if `indat' < 0.05 & "`stars'" == "stars" {
      return local pform =  string(`indat', "%4.2f")+"*"
    }
    else {
      return local pform =  string(`indat', "%4.2f")
    }
  }
end

* Subroutine for calculating standardized differences (borrowed from stdiff)
program papertab_runsd, rclass
  version 11
  syntax varname [if] [in], treatvar(varname) [weighted contfmt(string) pctfmt(string)]
	marksample touse
  if "`nweighted'" != "nweighted" {
    local poolsd_cont "sqrt(((\`s1')^2+(\`s2')^2) / 2)"
    local poolsd_bin "sqrt(((\`p1'*(1-\`p1'))+(\`p2'*(1-\`p2')))/2)"
  }

  else {
    local poolsd_cont "sqrt((\`=\`n1'-1'*(\`s1')^2+\`=\`n2'-1'*(\`s2')^2) /(\`n1'+\`n2'))"
    local poolsd_bin "sqrt((\`=\`n1'-1'*(\`p1'*(1-\`p1'))+\`=\`n2'-1'*(\`p2'*(1-\`p2'))) /(\`n1'+\`n2'))"
  }

  if "`contfmt'" =="" {
  	 local contfmt "%4.1f"
  }

  if "`pctfmt'" =="" {
  	 local pctfmt "%3.1f"
  }

  qui levelsof `treatvar' if `touse', local(tvlevels)

  * Weighting
  if "`weighted'" == "weighted" {
    capture assert (`varlist' == 0 | `varlist' == 1 | `varlist' == .) if `touse'
    if _rc == 9 {
      svy: mean `varlist' if `touse', over(`treatvar')
      qui estat sd

      foreach x in 1 2 {
        local y: word `x' of `tvlevels'
        local m`x' = el(r(mean),1,`x')
        local s`x' = el(r(sd),1,`x')
        local n`x' = el(e(_N_subp),1,`x')
        return local comb_`y' = string(`m`x'', "`contfmt'")+" ("+string(`s`x'',"`contfmt'")+")"
      }

      local sdpool = `poolsd_cont'
      local stdiff = (`m2'-`m1')/`sdpool'
      
      svy: mean `varlist' if `touse'
      qui estat sd
      return local comb_oa = string(el(r(mean),1,1), "`contfmt'")+" ("+string(el(r(sd),1,1),"`contfmt'")+")"
    }

    else {
      svy: mean `varlist' if `touse', over(`treatvar')
      qui estat sd

      foreach x in 1 2 {
        local y: word `x' of `tvlevels'
        local p`x' = el(r(mean),1,`x')
        local n`x' = el(e(_N_subp),1,`x')
        return local comb_`y' = string(`p`x''*100, "`pctfmt'")
      }

      local sdpool = `poolsd_bin'
      local stdiff = (`p2'-`p1')/`sdpool'

      svy: mean `varlist' if `touse'
      qui estat sd
      return local comb_oa = string(el(r(mean),1,1)*100, "`pctfmt'")
    }
  }
  
  * Unweighted
  else {
    capture assert (`varlist' == 0 | `varlist' == 1 | `varlist' == .) if `touse'
  
    if _rc == 9 {
      qui mean `varlist' if `touse', over(`treatvar')
      qui estat sd
  
      foreach x in 1 2 {
        local y: word `x' of `tvlevels'
        local m`x' = el(r(mean),1,`x')
        local s`x' = el(r(sd),1,`x')
        local n`x' = el(e(_N),1,`x')
        return local comb_`y' = string(`m`x'', "`contfmt'")+" ("+string(`s`x'',"`contfmt'")+")"
      }
  
      local sdpool = `poolsd_cont'
      local stdiff = (`m2'-`m1')/`sdpool'
      
      qui mean `varlist' if `touse'
      qui estat sd
      return local comb_oa = string(el(r(mean),1,1), "`contfmt'")+" ("+string(el(r(sd),1,1),"`contfmt'")+")"
  
    }
  
    else {
      qui mean `varlist' if `touse', over(`treatvar')
      qui estat sd
  
      foreach x in 1 2 { 
        local y: word `x' of `tvlevels'
        local p`x' = el(r(mean),1,`x')
        local n`x' = el(e(_N),1,`x')
        return local comb_`y' = string(`p`x''*100, "`pctfmt'")
      }
  
      local sdpool = `poolsd_bin'
      local stdiff = (`p2'-`p1')/`sdpool'
      
      mean `varlist' if `touse'
      qui estat sd
      return local comb_oa = string(el(r(mean),1,1)*100, "`pctfmt'")
    }
  
  }
  return local pform = string(abs(`stdiff')*100,"%4.1f")
end
