{smcl}
{* *! 0.0.10 Jerome Federspiel 19apr2013}{...}
{viewerjumpto "Syntax" "stdiff##syntax"}{...}
{viewerjumpto "Description" "stdiff##description"}{...}
{viewerjumpto "Required Parameters" "stdiff##required"}{...}
{viewerjumpto "Options" "stdiff##options"}{...}
{viewerjumpto "Examples" "stdiff##examples"}{...}
{viewerjumpto "References" "stdiff##references"}{...}
{viewerjumpto "Author" "stdiff##author"}{...}
{title:Title}

{phang}
{cmd:papertab} {hline 2} Create simple table of summary statistics for papers.

{marker syntax}{...}
{title:Syntax}

{p 8 17 2}
{cmdab:papertab}
{varlist}
, {cmdab:treat:var(varname)} [{it:options}]

{synoptset 20}{...}
{synopthdr:Required}
{synoptline}
{synopt:{varlist}}List of variables to summarize{p_end}
{synopt:{opt treat:var}}Variable defining different groups to compare {p_end}
{synoptline}
{p2colreset}{...}

{synoptset 20}{...}
{synopthdr}
{synoptline}
{synopt:{opt outtab:le(filename, replace append)}}Outputs results as tab-delimited file {p_end}
{synopt:{opt outrtf:(filename, replace rtflabelwidth(real) rtfcolwidth(real) rtfpwidth(real))}}Outputs results as RTF file {p_end}
{synopt:{opt stat:type(string)}}Type of test to perform ("parametric", "nonparm", "stdiff", "trend") {p_end}
{synopt:{opt level:override}}Runs table, even when there are more than 10 levels of treatvar() {p_end} 
{synopt:{opt pctf:mt(string)}}Display format for percentages (defaults to %4.1f (xx.x%)) {p_end}
{synopt:{opt contf:mt(string)}}Display format for non-percentage variables (defaults to %4.1f (1 decimal place)) {p_end}
{synopt:{opt colw:idth(integer)}}Width of columns of statistics (in characters) {p_end}
{synopt:{opt labw:idth(integer)}}Width of variable labels (in characters) {p_end}
{synopt:{opt svy}}Uses survey-weighted commands{p_end}
{synopt:{opt star:s}}Adds significance stars{p_end}
{synopt:{opt title:(string)}}Adds title to graph{p_end}
{synoptline}
{p 4 6 2}
{cmd:pweight}s are allowed; see {help weights}.

{p2colreset}{...}


{marker description}{...}
{title:Description}

{p 4 4 0 0}
{cmd:papertab} is designed to quickly produce a list of summary statistics comparing 2+ treatment groups.


{p2colreset}{...}
{marker required}{...}
{title:Required Parameters}

{phang}
{varlist} follows the usual Stata syntax to identify covariates to be summarized in the table.
Factor variables are supported (using the i. operator), but interaction terms are not. Binary variables
entered without an i. operator will be summarized using only one row (the larger of value (1 for 0/1 coding, 2 for 1/2 coding, etc)). To override 
this behavior (i.e., to have a row for the percentage equal to zero and a row for the percentage 
equal to 1), simply add a i. in front. Note: observations with missing values will be excluded from calculations (only for those variables with missing values)

{phang}
{opt treatvar} is used to specify the treatment variable over which the variables are to be summarized. Note: observations with missing values
for the treatment variable will be included in the Overall column, but will not be included in comparisons across values of the treatment variable.


{marker options}{...}
{title:Options}

{phang}
{opt outtable(filename, replace append skiptable)} is used to specify the name of a tab-delimited file to contain the results. This option
is particularly useful when exporting results to a word procesing or spreadsheet program for publication. The {it: replace} option replaces
an existing file with the same name. The {it: append} adds onto an existing file.

{phang}
{opt outrtf(filename, replace append skiptable)} is used to specify the name of a rich text file (RTF) to contain the results. This option
is particularly useful when exporting results to a word procesing or spreadsheet program for publication. The {it: replace} option replaces
an existing file with the same name. The {it: rtflabelwidth} option specifies the width of the label column (default is 2 inches). The 
{it: rtfcolwidth} option specifies the width of the data columns (default is 1 inches). The {it: rtfpvwidth} option specifies the width 
of the p-value column (default is 0.6 inches).

{phang}
{opt stattype} selects the type of statistical analysis to perform. The default ({opt parametric}) uses 
t-tests when treatvar is binary and ANOVA when treatvar has >2 groups (continuous variables) and
chi-2 (binary / categorical variables). When run with a [pweight], the program switches instead to 
weighted linear regression for continuous variables and a weighted chi2 test for binary / categorical variables.
The {opt nonparm} option substitutes Kruskal-Wallis tests for
continuous variables, and displays median [25th-75th percentile] in place of mean (standard deviation).
The {opt stdiff} option calculates standardized differences. If used, treatvar must be a binary variable (a
variable with only two levels).
The {opt trend} option uses a non-parametric test for trend (the stata {cmd: nptrend} command) for all 
variables and is appropriate when the {cmd: treatvar} is ordered. For {opt parametric} and {opt nonparm} 
options, variables specified with i. will have a single joint p-value reported, rather than pairwise 
comparisons between levels.

{phang}
{opt leveloverride} overrides the command's default checking of treatvar()'s levels. By default, the command
does not produce a table if treatvar has 10 or more unique levels, as this most likely reprents user error; 
leveloverride forces the command to produce the table anyways.

{phang}
{opt pctfmt()} is used to specify the format of percentage values, using standard Stata syntax. It defaults to %4.1f.

{phang}
{opt contfmt()} is used to specify the format of all non-percentage values, using standard Stata syntax. It defaults to %4.1f.

{phang}
{opt colwidth()} is used to specify the width of the columns of statistics (in characters). It defaults to 25.

{phang}
{opt labwidth()} is used to specify the width of the variable / value labels (in characters). It defaults to 35.

{phang}
{opt svy} produces weighted estimates (must be run after data are {cmd:svyset}). Cannot be run with stattype = nonparm or stattype = trend. 
Uses weighted linear regression for continuous variables (svy: reg), and weighted chi2 test for categorical variables (svy: tab). 

{phang}
{opt stars} adds asterisks for p-values <0.05 (*), <0.01 (**), and <0.001 (***) (svy: tab). 


{marker examples}{...}
{title:Example}

{phang}{cmd:// Set up}{p_end}
{phang}{cmd:sysuse auto, clear}{p_end}

{phang}{cmd:* Create categorical variable for demonstration}{p_end}
{phang}{cmd:set seed 1}{p_end}
{phang}{cmd:gen rand = runiform()}{p_end}
{phang}{cmd:sort rand}{p_end}

{phang}{cmd:gen color = (1+1*(_n>20)+1*(_n>40))}{p_end}
{phang}{cmd:label var color "Car Color"}{p_end}
{phang}{cmd:label define colorlab 1 "Blue" 2 "Green" 3 "Yellow"}{p_end}
{phang}{cmd:label val color colorlab}{p_end}

{phang}{cmd:* Run calculations (command should be on single line)}{p_end}
{phang}{cmd:papertab price mpg trunk displacement i.color gear_ratio length turn, treatvar(foreign) stattype(nonparm) outtable("test.tsv", replace) pctfmt(%3.0f) labwidth(40)}{p_end}


{marker references}{...}
{title:References}

{phang} None


{marker author}{...}
{title:Author}

{phang}
Jerome Federspiel, University of North Carolina at Chapel Hill. {browse "jerome_federspiel@med.unc.edu"} {p_end}


